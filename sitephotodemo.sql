-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 21 Février 2019 à 10:02
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sitephotodemo`
--

-- --------------------------------------------------------

--
-- Structure de la table `commentary`
--

CREATE TABLE `commentary` (
  `id` int(11) NOT NULL,
  `comment` varchar(8000) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `work_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `commentary`
--

INSERT INTO `commentary` (`id`, `comment`, `user_id`, `work_id`) VALUES
(28, 'Wouaou trop top!', 3, 4),
(29, 'Ouais bof...', 2, 4),
(30, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `imageRef_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `gallery`
--

INSERT INTO `gallery` (`id`, `description`, `title`, `imageRef_id`) VALUES
(32, 'Vacances, voyages, far niente.', 'Vacances', 4),
(33, 'ARCHI', 'Architecture', 10),
(34, 'Randonnées dans le Mercantour, Côte d\'Azur, Piémont italien', 'Randos', 17),
(35, 'Portraits divers.', 'Portraits', 23);

-- --------------------------------------------------------

--
-- Structure de la table `gallery_work`
--

CREATE TABLE `gallery_work` (
  `setGalleries_id` int(11) NOT NULL,
  `setWorkGal_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `gallery_work`
--

INSERT INTO `gallery_work` (`setGalleries_id`, `setWorkGal_id`) VALUES
(32, 4),
(32, 5),
(32, 6),
(32, 7),
(32, 8),
(32, 9),
(33, 10),
(33, 11),
(33, 12),
(33, 13),
(33, 14),
(33, 15),
(34, 16),
(34, 17),
(34, 18),
(34, 19),
(34, 20),
(34, 21),
(35, 22),
(35, 23),
(35, 24),
(35, 25),
(35, 26),
(35, 27);

-- --------------------------------------------------------

--
-- Structure de la table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(39),
(39),
(39),
(39);

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE `image` (
  `forSale` bit(1) DEFAULT NULL,
  `nbSold` int(11) DEFAULT NULL,
  `refInstagram` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `image`
--

INSERT INTO `image` (`forSale`, `nbSold`, `refInstagram`, `id`) VALUES
(b'0', 0, NULL, 4),
(b'0', 0, NULL, 5),
(b'0', 0, NULL, 6),
(b'0', 0, NULL, 7),
(b'0', 0, NULL, 8),
(b'0', 0, NULL, 9),
(b'0', 0, NULL, 10),
(b'0', 0, NULL, 11),
(b'0', 0, NULL, 12),
(b'0', 0, NULL, 13),
(b'0', 0, NULL, 14),
(b'0', 0, NULL, 15),
(b'0', 0, NULL, 16),
(b'0', 0, NULL, 17),
(b'0', 0, NULL, 18),
(b'0', 0, NULL, 19),
(b'0', 0, NULL, 20),
(b'0', 0, NULL, 21),
(b'0', 0, NULL, 22),
(b'0', 0, NULL, 23),
(b'0', 0, NULL, 24),
(b'0', 0, NULL, 25),
(b'0', 0, NULL, 26),
(b'0', 0, NULL, 27);

-- --------------------------------------------------------

--
-- Structure de la table `model`
--

CREATE TABLE `model` (
  `modelType` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `model`
--

INSERT INTO `model` (`modelType`, `id`) VALUES
('bystander', 31);

-- --------------------------------------------------------

--
-- Structure de la table `model_work`
--

CREATE TABLE `model_work` (
  `setModels_id` int(11) NOT NULL,
  `setWorkMod_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `model_work`
--

INSERT INTO `model_work` (`setModels_id`, `setWorkMod_id`) VALUES
(31, 4),
(31, 5),
(31, 6),
(31, 7),
(31, 8),
(31, 9);

-- --------------------------------------------------------

--
-- Structure de la table `person`
--

CREATE TABLE `person` (
  `id` int(11) NOT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `person`
--

INSERT INTO `person` (`id`, `firstName`, `lastName`) VALUES
(1, 'T', 'T'),
(2, 'Alain', 'Deloin'),
(3, 'Jean', 'Valjean'),
(31, 'Cozette', 'La'),
(36, 'r', 'r'),
(37, 'y', 'y'),
(38, 'u', 'u');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `login` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `userRole` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `userGallery_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`login`, `password`, `userRole`, `id`, `userGallery_id`) VALUES
('tt', 'tt', 'admin', 1, NULL),
('Alain', 'Deloin', 'simpleUser', 2, NULL),
('Jean', 'Valjean', 'simpleUser', 3, NULL),
('r', 'r', 'simpleUser', 36, NULL),
('y', 'y', 'simpleUser', 37, NULL),
('u', 'u', 'simpleUser', 38, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `user_work`
--

CREATE TABLE `user_work` (
  `setUsersAccess_id` int(11) NOT NULL,
  `setWorkUser_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `work`
--

CREATE TABLE `work` (
  `id` int(11) NOT NULL,
  `dateTaken` datetime(6) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `locationTaken` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `urlLocation` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `work`
--

INSERT INTO `work` (`id`, `dateTaken`, `description`, `locationTaken`, `title`, `urlLocation`) VALUES
(4, '2018-12-06 18:30:40.389000', 'Taken there', 'Monaco', 'vacances 01', 'images/vacances01.jpg'),
(5, '2018-12-06 18:30:40.389000', 'Taken wherever', 'Monaco', 'vacances 02', 'images/vacances02.jpg'),
(6, '2018-12-06 18:30:40.389000', 'Taken here', 'Cannes', 'vacances 03', 'images/vacances03.jpg'),
(7, '2018-12-06 18:30:40.389000', 'Taken there', 'La Baule', 'vacances 04', 'images/vacances04.jpg'),
(8, NULL, 'Heu Heu...', 'Bombay', 'vacances 05', 'images/vacances05.jpg'),
(9, '2018-12-06 18:30:40.389000', 'Heu Heu... Aaargh...', 'Marseille', 'vacances 06', 'images/vacances06.jpg'),
(10, '2018-12-06 18:30:40.389000', NULL, NULL, 'architecture 01', 'images/architecture01.jpg'),
(11, '2018-12-06 18:30:40.389000', 'Taken wherever', NULL, 'architecture 02', 'images/architecture02.jpg'),
(12, '2018-12-06 18:30:40.389000', 'Taken here', NULL, 'architecture 03', 'images/architecture03.jpg'),
(13, '2018-12-06 18:30:40.389000', NULL, NULL, 'architecture 04', 'images/architecture04.jpg'),
(14, NULL, NULL, NULL, 'architecture 05', 'images/architecture05.jpg'),
(15, NULL, NULL, NULL, 'architecture 06', 'images/architecture06.jpg'),
(16, '2018-12-06 18:30:40.389000', NULL, 'Piemont', 'randos 01', 'images/randos01.jpg'),
(17, '2018-12-06 18:30:40.389000', NULL, 'Mercantour', 'randos 02', 'images/randos02.jpg'),
(18, '2018-12-06 18:30:40.389000', NULL, 'Mercantour', 'randos 03', 'images/randos03.jpg'),
(19, '2018-12-06 18:30:40.389000', NULL, 'Mercantour', 'randos 04', 'images/randos04.jpg'),
(20, NULL, NULL, 'Mercantour', 'randos 05', 'images/randos05.jpg'),
(21, '2018-12-06 18:30:40.389000', NULL, 'Mercantour', 'randos 06', 'images/randos06.jpg'),
(22, '2018-12-06 18:30:40.389000', NULL, NULL, 'portraits 01', 'images/portraits01.jpg'),
(23, '2018-12-06 18:30:40.389000', NULL, NULL, 'portraits 02', 'images/portraits02.jpg'),
(24, '2018-12-06 18:30:40.389000', NULL, NULL, 'portraits 03', 'images/portraits03.jpg'),
(25, '2018-12-06 18:30:40.389000', NULL, NULL, 'portraits 04', 'images/portraits04.jpg'),
(26, NULL, NULL, NULL, 'portraits 05', 'images/portraits05.jpg'),
(27, '2018-12-06 18:30:40.389000', NULL, NULL, 'portraits 06', 'images/portraits06.jpg');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `commentary`
--
ALTER TABLE `commentary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK4gnkpacv6jppejtkqdef1fkq4` (`user_id`),
  ADD KEY `FK8injyat1tab3097seo0m0qnx7` (`work_id`);

--
-- Index pour la table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKhoo0xqlv1bg0smp5cath5k0el` (`imageRef_id`);

--
-- Index pour la table `gallery_work`
--
ALTER TABLE `gallery_work`
  ADD PRIMARY KEY (`setGalleries_id`,`setWorkGal_id`),
  ADD KEY `FKpyscxaqlpj4cedwsupxq00hdw` (`setWorkGal_id`);

--
-- Index pour la table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `model`
--
ALTER TABLE `model`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `model_work`
--
ALTER TABLE `model_work`
  ADD PRIMARY KEY (`setModels_id`,`setWorkMod_id`),
  ADD KEY `FKpcuokh0ykfvtvx5rs14a0yaqt` (`setWorkMod_id`);

--
-- Index pour la table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK3qd5cagydl7xkyoc7lpctwr88` (`userGallery_id`);

--
-- Index pour la table `user_work`
--
ALTER TABLE `user_work`
  ADD PRIMARY KEY (`setUsersAccess_id`,`setWorkUser_id`),
  ADD KEY `FK7vgsk826uc1xo888y067gjc1f` (`setWorkUser_id`);

--
-- Index pour la table `work`
--
ALTER TABLE `work`
  ADD PRIMARY KEY (`id`);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `commentary`
--
ALTER TABLE `commentary`
  ADD CONSTRAINT `FK4gnkpacv6jppejtkqdef1fkq4` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK8injyat1tab3097seo0m0qnx7` FOREIGN KEY (`work_id`) REFERENCES `work` (`id`);

--
-- Contraintes pour la table `gallery`
--
ALTER TABLE `gallery`
  ADD CONSTRAINT `FKhoo0xqlv1bg0smp5cath5k0el` FOREIGN KEY (`imageRef_id`) REFERENCES `image` (`id`);

--
-- Contraintes pour la table `gallery_work`
--
ALTER TABLE `gallery_work`
  ADD CONSTRAINT `FKaod1fii5pborvjch6ayj82ke8` FOREIGN KEY (`setGalleries_id`) REFERENCES `gallery` (`id`),
  ADD CONSTRAINT `FKpyscxaqlpj4cedwsupxq00hdw` FOREIGN KEY (`setWorkGal_id`) REFERENCES `work` (`id`);

--
-- Contraintes pour la table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `FKbbby0axw2c7wpm3di7yebtxe8` FOREIGN KEY (`id`) REFERENCES `work` (`id`);

--
-- Contraintes pour la table `model`
--
ALTER TABLE `model`
  ADD CONSTRAINT `FKsy6duig8ywo0j303stuqwo7ee` FOREIGN KEY (`id`) REFERENCES `person` (`id`);

--
-- Contraintes pour la table `model_work`
--
ALTER TABLE `model_work`
  ADD CONSTRAINT `FKpcuokh0ykfvtvx5rs14a0yaqt` FOREIGN KEY (`setWorkMod_id`) REFERENCES `work` (`id`),
  ADD CONSTRAINT `FKs3lb3wsocg1oja0k82vx8cp44` FOREIGN KEY (`setModels_id`) REFERENCES `model` (`id`);

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK3qd5cagydl7xkyoc7lpctwr88` FOREIGN KEY (`userGallery_id`) REFERENCES `gallery` (`id`),
  ADD CONSTRAINT `FKcjpqttwpa4utc5mn8rhybca2r` FOREIGN KEY (`id`) REFERENCES `person` (`id`);

--
-- Contraintes pour la table `user_work`
--
ALTER TABLE `user_work`
  ADD CONSTRAINT `FK7vgsk826uc1xo888y067gjc1f` FOREIGN KEY (`setWorkUser_id`) REFERENCES `work` (`id`),
  ADD CONSTRAINT `FKg29mrwcybkhfcd1w6sa4ihj7o` FOREIGN KEY (`setUsersAccess_id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
