package com.ttabphoto.sitePhotoBack;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import config.HibernateConf;
import services.Service;

public class App 
{
    public static void main( String[] args ) {
    	

		
		// Session s = HibernateUtil.getSessionFactory().openSession();
		
//		User user01 = new User();
//		user01.setFirstName("Jean");
//		user01.setLastName("Valjean");
//		user01.setLogin("Jean");
//		user01.setPassword("Valjean");
//		user01.setUserRole(UserRole.simpleUser);
//		
//		User user02 = new User();
//		user02.setFirstName("Alain");
//		user02.setLastName("Deloin");
//		user02.setLogin("Alain");
//		user02.setPassword("Deloin");
//		user02.setUserRole(UserRole.admin);
//		
//		UserDAO userDAO = new UserDAO();
//		userDAO.setSession(s);
//		userDAO.save(user01);
//		userDAO.save(user02);
//		
//		//
//		
//		Image i1 = new Image();
//		i1.setTitle("photo1");
//		Date date = new Date();
//		i1.setDateTaken(date);
//		i1.setUrlLocation("https://proxy.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.wallpapers13.com%2Fwp-content%2Fuploads%2F2016%2F01%2FMountain-scenery-with-snow-covered-river-rocks-beautiful-Hd-wallpaper-915x515.jpg&f=1");
//		i1.setDescription("Taken there");
//		
//		Image i2 = new Image();
//		i2.setTitle("photo2");
//		i2.setDateTaken(date);
//		i2.setUrlLocation("https://proxy.duckduckgo.com/iu/?u=http%3A%2F%2F4.bp.blogspot.com%2F-tcIOYxa5akg%2FT-spBfyFBZI%2FAAAAAAAAAyQ%2FQ6zLAg_mFoQ%2Fs1600%2Fnatural-scenery-7.jpg&f=1");
//		i2.setDescription("Taken wherever");
//		
//		Image i3 = new Image();
//		i3.setTitle("photo3");
//		i3.setDateTaken(date);
//		i3.setUrlLocation("https://proxy.duckduckgo.com/iu/?u=http%3A%2F%2F1.bp.blogspot.com%2F-DDC0yUkzDY0%2FUT37NMYS1JI%2FAAAAAAAAUZA%2FcJaVU7qmjWU%2Fs1600%2FNature%2BScenery%2BWallpapers%2B2.jpg&f=1");
//		i3.setDescription("Taken here");
//		
//		ImageDAO imageDAO = new ImageDAO();
//		imageDAO.setSession(s);
//		imageDAO.save(i1);
//		imageDAO.save(i2);
//		imageDAO.save(i3);
//		
//		Set<Work> setWorkGal1 = new HashSet<Work>();
//		setWorkGal1.add(i1);
//		setWorkGal1.add(i2);
//		setWorkGal1.add(i3);
//		Set<Work> setWorkGal2 = new HashSet<Work>();
//		setWorkGal2.add(i1);
//
//		
//		//
//		
//		Gallery gal1 = new Gallery();
//		gal1.setTitle("Gallery1");
//		gal1.setSetWork(setWorkGal1);
//		
//		Gallery gal2 = new Gallery();
//		gal2.setTitle("Gallery2");
//		gal2.setSetWork(setWorkGal2);
//		gal2.setUser(user01);
//		
//		GalleryDAO galleryDAO = new GalleryDAO();
//		galleryDAO.setSession(s);
//		galleryDAO.save(gal1);
//		galleryDAO.save(gal2);
//		
//
//		
//		//
//		
//		Commentary c1 = new Commentary();		
//		c1.setUser(user01);
//		c1.setComment("Wouaou trop top!");
//		c1.setWork(i1);
//		Commentary c2 = new Commentary();
//		c2.setUser(user01);
//		c2.setComment("Finalement non.");
//		c2.setWork(i1);
//		
//		CommentaryDAO commentaryDAO = new CommentaryDAO();
//		commentaryDAO.setSession(s);
//		commentaryDAO.save(c1);
//		commentaryDAO.save(c2);
//
//		
//		//
//		
//		Model mod1 = new Model();
//		mod1.setFirstName("Cozette");
//		mod1.setLastName("La");
//		mod1.setModelType(ModelType.bystander);
//		mod1.setSetWork(setWorkGal2);
//
//		ModelDAO modelDAO = new ModelDAO();
//		modelDAO.setSession(s);
//		modelDAO.save(mod1);
//		
//		//update users and images
////		userDAO.save(user01);
////		userDAO.save(user02);
////		imageDAO.save(i1);
////		imageDAO.save(i2);
////		imageDAO.save(i3);
//		
//		//
//		
//		userDAO.getSession().close();
//		imageDAO.getSession().close();
//		galleryDAO.getSession().close();
//		commentaryDAO.getSession().close();
//		modelDAO.getSession().close();
    	
    	ApplicationContext context = new AnnotationConfigApplicationContext(HibernateConf.class);
		Service srv = (Service) context.getBean(Service.class);
		
		srv.run();
        
    }
}
