package services;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import dao.ImageDAO;
import dao.UserDAO;

@Component
public class Service {

	@Autowired
	SessionFactory sf;
	
	@Autowired
	ImageDAO imageDAO;
	
	@Autowired
	UserDAO userDAO;
	
	@Transactional
	public void run() {
		imageDAO.setSession(sf.openSession());
		System.out.println(imageDAO.findAll());
		userDAO.setSession(sf.openSession());
		System.out.println(userDAO.findAll());
	}
	
	
}
