package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Commentary {

	@Id
	@GeneratedValue
	private int id;
	
	@Column(length = 8000)
	private String comment;
	
	@ManyToOne//(fetch=FetchType.EAGER)
	@JoinColumn
	private User user;
	
	@ManyToOne//(fetch=FetchType.EAGER)
	@JoinColumn//(name="WORK_ID")
	private Work work;

	
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	@XmlID
	public String getStringId() {
		return ""+getId();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@XmlIDREF
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	//@XmlIDREF
	public Work getWork() {
		return work;
	}
	public void setWork(Work work) {
		this.work = work;
	}
	

	public Commentary() {}

}
