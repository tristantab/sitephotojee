package models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.persistence.Column;


@Entity
@PrimaryKeyJoinColumn(name="id")
@Table
@XmlRootElement
public class Model extends Person {

	@Id
	@GeneratedValue
	private int id;
	
	// type of model or contract
	@Enumerated(EnumType.STRING)
	@Column
	private ModelType modelType;
	
	@ManyToMany
	@JoinTable
	private Set<Work> setWork = new HashSet<Work>();
	
	//????@XmlIDREF 
	public ModelType getModelType() {
		return modelType;
	}
	public void setModelType(ModelType modelType) {
		this.modelType = modelType;
	}

	@XmlIDREF
	public Set<Work> getSetWork() {
		return setWork;
	}
	public void setSetWork(Set<Work> setWork) {
		this.setWork = setWork;
	}

	@XmlID
	public String getStringId() {
		return ""+getId();
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	public Model() {}

}
