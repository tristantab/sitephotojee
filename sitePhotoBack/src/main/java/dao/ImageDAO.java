package dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import models.Image;

@Repository
public class ImageDAO extends GenericDAO<Image>{

	public ImageDAO() {
		super(Image.class);
	}

	
	public List<Image> findByTitle(String titleTry) {
		List<Image> ret;
		Query q = s.createQuery("from Image where title like :title");
		q.setParameter("title", "%"+titleTry+"%");
		ret = q.getResultList();
		return ret;
	}

	public List<Image> findByTitreAndAuteur(String titleTry, String locationTakenTry) {
			System.out.println("titre ["+titleTry+"] auteur ["+locationTakenTry+"]");
		List<Image> ret;
		Query q = s.createQuery("from Image where title like :title and locationTaken like :locationTaken");
		q.setParameter("title", "%"+titleTry+"%");
		q.setParameter("location", "%"+locationTakenTry+"%");
		ret = q.getResultList();
		return ret;
	}
	
	
	
}
