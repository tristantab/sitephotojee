package com.ttabphoto.sitePhotoBack;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


import config.PersistenceConfig;
import models.Commentary;
import models.Gallery;
import models.Image;
import models.Model;
import models.ModelType;
import models.User;
import models.UserRole;
import models.Work;
import repositories.CommentaryRepository;
import repositories.GalleryRepository;
import repositories.ImageRepository;
import repositories.ModelRepository;
import repositories.UserRepository;

public class App 
{
    public static void main( String[] args ) {
    	
		ApplicationContext context = new AnnotationConfigApplicationContext(PersistenceConfig.class);
    	
		
//    	
//    	// Defining original data
//    	
//		User user01 = new User();
//		user01.setFirstName("T");
//		user01.setLastName("T");
//		user01.setLogin("tt");
//		user01.setPassword("tt");
//		user01.setUserRole(UserRole.admin);
//		
//		User user02 = new User();
//		user02.setFirstName("Alain");
//		user02.setLastName("Deloin");
//		user02.setLogin("Alain");
//		user02.setPassword("Deloin");
//		user02.setUserRole(UserRole.simpleUser);
//		
//		User user03 = new User();
//		user03.setFirstName("Jean");
//		user03.setLastName("Valjean");
//		user03.setLogin("Jean");
//		user03.setPassword("Valjean");
//		user03.setUserRole(UserRole.simpleUser);
//		
//		UserRepository userRepo = (UserRepository) context.getBean(UserRepository.class);
//		userRepo.save(user01);
//		userRepo.save(user02);
//		userRepo.save(user03);
//
//		Date date = new Date();
//		
//		Image i1 = new Image();
//		i1.setTitle("vacances 01");
//
//		i1.setDateTaken(date);
//		i1.setUrlLocation("images/vacances01.jpg");
//		i1.setDescription("Taken there");
//		i1.setLocationTaken("Monaco");
//		
//		Image i2 = new Image();
//		i2.setTitle("vacances 02");
//		i2.setDateTaken(date);
//		i2.setUrlLocation("images/vacances02.jpg");
//		i2.setDescription("Taken wherever");
//		i2.setLocationTaken("Monaco");
//		
//		Image i3 = new Image();
//		i3.setTitle("vacances 03");
//		i3.setDateTaken(date);
//		i3.setUrlLocation("images/vacances03.jpg");
//		i3.setDescription("Taken here");
//		i3.setLocationTaken("Cannes");
//		
//		Image i4 = new Image();
//		i4.setTitle("vacances 04");
//		i4.setDateTaken(date);
//		i4.setUrlLocation("images/vacances04.jpg");
//		i4.setDescription("Taken there");
//		i4.setLocationTaken("La Baule");
//		
//		Image i5 = new Image();
//		i5.setTitle("vacances 05");
//		i5.setUrlLocation("images/vacances05.jpg");
//		i5.setDescription("Heu Heu...");
//		i5.setLocationTaken("Bombay");
//		
//		Image i6 = new Image();
//		i6.setTitle("vacances 06");
//		i6.setDateTaken(date);
//		i6.setUrlLocation("images/vacances06.jpg");
//		i6.setDescription("Heu Heu... Aaargh...");
//		i6.setLocationTaken("Marseille");
//		
//		Image ia1 = new Image();
//		ia1.setTitle("architecture 01");
//		ia1.setDateTaken(date);
//		ia1.setUrlLocation("images/architecture01.jpg");
//		
//		Image ia2 = new Image();
//		ia2.setTitle("architecture 02");
//		ia2.setDateTaken(date);
//		ia2.setUrlLocation("images/architecture02.jpg");
//		ia2.setDescription("Taken wherever");
//		
//		Image ia3 = new Image();
//		ia3.setTitle("architecture 03");
//		ia3.setDateTaken(date);
//		ia3.setUrlLocation("images/architecture03.jpg");
//		ia3.setDescription("Taken here");
//		
//		Image ia4 = new Image();
//		ia4.setTitle("architecture 04");
//		ia4.setDateTaken(date);
//		ia4.setUrlLocation("images/architecture04.jpg");
//		
//		Image ia5 = new Image();
//		ia5.setTitle("architecture 05");
//		ia5.setUrlLocation("images/architecture05.jpg");
//		
//		Image ia6 = new Image();
//		ia6.setTitle("architecture 06");
//		ia6.setUrlLocation("images/architecture06.jpg");
//		
//		Image ib1 = new Image();
//		ib1.setTitle("randos 01");
//		ib1.setDateTaken(date);
//		ib1.setUrlLocation("images/randos01.jpg");
//		ib1.setLocationTaken("Piemont");
//		
//		Image ib2 = new Image();
//		ib2.setTitle("randos 02");
//		ib2.setDateTaken(date);
//		ib2.setUrlLocation("images/randos02.jpg");
//		ib2.setLocationTaken("Mercantour");
//		
//		Image ib3 = new Image();
//		ib3.setTitle("randos 03");
//		ib3.setDateTaken(date);
//		ib3.setUrlLocation("images/randos03.jpg");
//		ib3.setLocationTaken("Mercantour");
//		
//		Image ib4 = new Image();
//		ib4.setTitle("randos 04");
//		ib4.setDateTaken(date);
//		ib4.setUrlLocation("images/randos04.jpg");
//		ib4.setLocationTaken("Mercantour");
//		
//		Image ib5 = new Image();
//		ib5.setTitle("randos 05");
//		ib5.setUrlLocation("images/randos05.jpg");
//		ib5.setLocationTaken("Mercantour");
//		
//		Image ib6 = new Image();
//		ib6.setTitle("randos 06");
//		ib6.setDateTaken(date);
//		ib6.setUrlLocation("images/randos06.jpg");
//		ib6.setLocationTaken("Mercantour");
//
//		Image ic1 = new Image();
//		ic1.setTitle("portraits 01");
//		ic1.setDateTaken(date);
//		ic1.setUrlLocation("images/portraits01.jpg");
//
//		
//		Image ic2 = new Image();
//		ic2.setTitle("portraits 02");
//		ic2.setDateTaken(date);
//		ic2.setUrlLocation("images/portraits02.jpg");
//		
//		Image ic3 = new Image();
//		ic3.setTitle("portraits 03");
//		ic3.setDateTaken(date);
//		ic3.setUrlLocation("images/portraits03.jpg");
//	
//		Image ic4 = new Image();
//		ic4.setTitle("portraits 04");
//		ic4.setDateTaken(date);
//		ic4.setUrlLocation("images/portraits04.jpg");
//		
//		Image ic5 = new Image();
//		ic5.setTitle("portraits 05");
//		ic5.setUrlLocation("images/portraits05.jpg");
//		
//		Image ic6 = new Image();
//		ic6.setTitle("portraits 06");
//		ic6.setDateTaken(date);
//		ic6.setUrlLocation("images/portraits06.jpg");
//		
//		ImageRepository imageRepo = (ImageRepository) context.getBean(ImageRepository.class);
//		imageRepo.save(i1);
//		imageRepo.save(i2);
//		imageRepo.save(i3);
//		imageRepo.save(i4);
//		imageRepo.save(i5);
//		imageRepo.save(i6);
//		imageRepo.save(ia1);
//		imageRepo.save(ia2);
//		imageRepo.save(ia3);
//		imageRepo.save(ia4);
//		imageRepo.save(ia5);
//		imageRepo.save(ia6);
//		imageRepo.save(ib1);
//		imageRepo.save(ib2);
//		imageRepo.save(ib3);
//		imageRepo.save(ib4);
//		imageRepo.save(ib5);
//		imageRepo.save(ib6);
//		imageRepo.save(ic1);
//		imageRepo.save(ic2);
//		imageRepo.save(ic3);
//		imageRepo.save(ic4);
//		imageRepo.save(ic5);
//		imageRepo.save(ic6);
//		
//		Set<Work> setWorkGal1 = new HashSet<Work>();
//		setWorkGal1.add(i1);
//		setWorkGal1.add(i2);
//		setWorkGal1.add(i3);
//		setWorkGal1.add(i4);
//		setWorkGal1.add(i5);
//		setWorkGal1.add(i6);
//		Set<Work> setWorkGal2 = new HashSet<Work>();
//		setWorkGal2.add(ia1);
//		setWorkGal2.add(ia2);
//		setWorkGal2.add(ia3);
//		setWorkGal2.add(ia4);
//		setWorkGal2.add(ia5);
//		setWorkGal2.add(ia6);
//				
//		Set<Work> setWorkGal9 = new HashSet<Work>();
//		setWorkGal9.add(ib1);
//		setWorkGal9.add(ib2);
//		setWorkGal9.add(ib3);
//		setWorkGal9.add(ib4);
//		setWorkGal9.add(ib5);
//		setWorkGal9.add(ib6);
//		
//		Set<Work> setWorkGal10 = new HashSet<Work>();
//		setWorkGal10.add(ic1);
//		setWorkGal10.add(ic2);
//		setWorkGal10.add(ic3);
//		setWorkGal10.add(ic4);
//		setWorkGal10.add(ic5);
//		setWorkGal10.add(ic6);
//		
//		
//		Commentary c1 = new Commentary();		
//		c1.setUser(user03);
//		c1.setComment("Wouaou trop top!");
//		c1.setWork(i1);
//		Commentary c2 = new Commentary();
//		c2.setUser(user03);
//		c2.setComment("Finalement non.");
//		c2.setWork(i1);
//		Commentary c3 = new Commentary();
//		c2.setUser(user02);
//		c2.setComment("Ouais bof...");
//		c2.setWork(i1);
//			
//		CommentaryRepository commentaryRepo = (CommentaryRepository) context.getBean(CommentaryRepository.class);
//		commentaryRepo.save(c1);
//		commentaryRepo.save(c2);
//		commentaryRepo.save(c3);
//		
//		Model mod1 = new Model();
//		mod1.setFirstName("Cozette");
//		mod1.setLastName("La");
//		mod1.setModelType(ModelType.bystander);
//		mod1.setSetWorkMod(setWorkGal1);	
//		
//		ModelRepository modelRepo = (ModelRepository) context.getBean(ModelRepository.class);
//		modelRepo.save(mod1);
//		
//		Gallery gal1 = new Gallery();
//		gal1.setTitle("Vacances");
//		gal1.setSetWorkGal(setWorkGal1);
//		gal1.setImageRef(i1);
//		gal1.setDescription("Vacances, voyages, far niente.");
//		
//		Gallery gal2 = new Gallery();
//		gal2.setTitle("Architecture");
//		gal2.setSetWorkGal(setWorkGal2);
//		gal2.setUser(user01);
//		gal2.setImageRef(ia1);
//		gal2.setDescription("ARCHI");
//		
//		Gallery gal9 = new Gallery();
//		gal9.setTitle("Randos");
//		gal9.setSetWorkGal(setWorkGal9);
//		gal9.setImageRef(ib2);
//		gal9.setDescription("Randonnées dans le Mercantour, Côte d'Azur, Piémont italien");
//		
//		Gallery gal10 = new Gallery();
//		gal10.setTitle("Portraits");
//		gal10.setSetWorkGal(setWorkGal10);
//		gal10.setImageRef(ic2);
//		gal10.setDescription("Portraits divers.");
//		
//		GalleryRepository galRepo = (GalleryRepository) context.getBean(GalleryRepository.class);
//		galRepo.save(gal1);
//		galRepo.save(gal2);
//		galRepo.save(gal9);
//		galRepo.save(gal10);
//		
//			
		
//		List<Gallery> gals = new ArrayList<Gallery>();
//		gals = galRepo.findByTitleContaining("Gal");
//		for (Iterator iterator = gals.iterator(); iterator.hasNext();) {
//			Gallery gallery = (Gallery) iterator.next();
//			System.out.println(gallery.getTitle());
//		}
		
		
    }
}
