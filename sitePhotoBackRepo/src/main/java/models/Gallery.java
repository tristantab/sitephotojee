package models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table
@XmlRootElement
public class Gallery {

	@Id
	@GeneratedValue
	private int id;
	
	@Column
	private String title;
	
	@Column
	private String description;
	
	
	@ManyToOne//(fetch=FetchType.EAGER)
	@JoinColumn
	private Image imageRef;
	
	@ManyToMany(fetch=FetchType.EAGER) //cascade=CascadeType.ALL, 
	@JoinTable
	private Set<Work> setWorkGal = new HashSet<Work>();
	
	// user gallery
	@OneToOne(mappedBy="userGallery")
	private User user;
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	@XmlIDREF
	public Image getImageRef() {
		return imageRef;
	}
	public void setImageRef(Image imageRef) {
		this.imageRef = imageRef;
	}

	@XmlIDREF
	public Set<Work> getSetWorkGal() {
		return setWorkGal;
	}
	public void setSetWorkGal(Set<Work> setWork) {
		this.setWorkGal = setWork;
	}

	@XmlIDREF
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	@XmlID
	public String getStringId() {
		return ""+getId();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	
	public Gallery() {}

}
