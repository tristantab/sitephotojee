package models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@PrimaryKeyJoinColumn(name="id")
@Table
@XmlRootElement
public class Image extends Work{

	@Id
	@GeneratedValue
	private int id;
	
	@Column
	private String refInstagram;
	
	@Column
	private boolean forSale;
	
	@Column
	private int nbSold;
	
	// list of galleries as imageRef
	@OneToMany(mappedBy="imageRef")
	protected Set<Gallery> setGalleries = new HashSet<Gallery>();
	

	public String getRefInstagram() {
		return refInstagram;
	}
	public void setRefInstagram(String refInstagram) {
		this.refInstagram = refInstagram;
	}
	
	public boolean isForSale() {
		return forSale;
	}
	public void setForSale(boolean forSale) {
		this.forSale = forSale;
	}

	public int getNbSold() {
		return nbSold;
	}
	public void setNbSold(int nbSold) {
		this.nbSold = nbSold;
	}	

	@XmlID
	public String getStringId() {
		return ""+getId();
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	@XmlIDREF
	public Set<Gallery> getSetGalleries() {
		return setGalleries;
	}
	public void setSetGalleries(Set<Gallery> setGalleries) {
		this.setGalleries = setGalleries;
	}
	public Image() {}

	
}
