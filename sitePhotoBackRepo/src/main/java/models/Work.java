package models;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table
@XmlRootElement
public abstract class Work {

	@Id
	@GeneratedValue
	@Column
	private int id;
	
	@Column
	protected String title;
	
	@Column
	protected String locationTaken;
	
	@Column
	protected Date dateTaken;
	
	@Column
	protected String description;
	
	@Column
	protected String urlLocation;

	
	// list of galleries where it appears
	@ManyToMany(mappedBy = "setWorkGal")
	protected Set<Gallery> setGalleries = new HashSet<Gallery>();
	
	// list of users commentaries
	@OneToMany(mappedBy="work")
	protected Set<Commentary> setCommentaries = new HashSet<Commentary>();
		
	// list of users with access right
	@ManyToMany(mappedBy = "setWorkUser")
	protected Set<User> setUsersAccess = new HashSet<User>();
	
	// list of models
	@ManyToMany(mappedBy = "setWorkMod")
	protected Set<Model> setModels = new HashSet<Model>();
		
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getLocationTaken() {
		return locationTaken;
	}
	public void setLocationTaken(String locationTaken) {
		this.locationTaken = locationTaken;
	}

	public Date getDateTaken() {
		return dateTaken;
	}
	public void setDateTaken(Date dateTaken) {
		this.dateTaken = dateTaken;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrlLocation() {
		return urlLocation;
	}
	public void setUrlLocation(String urlLocation) {
		this.urlLocation = urlLocation;
	}

	@XmlIDREF
	public Set<Gallery> getSetGalleries() {
		return setGalleries;
	}
	public void setSetGalleries(Set<Gallery> setGalleries) {
		this.setGalleries = setGalleries;
	}
		
	@XmlIDREF
	public Set<Commentary> getSetCommentaries() {
		return setCommentaries;
	}
	public void setSetCommentaries(Set<Commentary> setCommentaries) {
		this.setCommentaries = setCommentaries;
	}
	
	@XmlIDREF
	public Set<User> getSetUsersAccess() {
		return setUsersAccess;
	}
	public void setSetUsersAccess(Set<User> setUsersAccess) {
		this.setUsersAccess = setUsersAccess;
	}
	
	@XmlIDREF
	public Set<Model> getSetModels() {
		return setModels;
	}
	public void setSetModels(Set<Model> setModels) {
		this.setModels = setModels;
	}
	
	@XmlID
	public String getStringId() {
		return ""+getId();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	public Work() {}
	

}
