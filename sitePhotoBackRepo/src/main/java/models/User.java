package models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@PrimaryKeyJoinColumn(name="id")
@Table
@XmlRootElement
public class User extends Person {

	@Id
	@GeneratedValue
	private int id;
	
	@Column
	private String login;
	
	@Column
	private String password;
	
	@Enumerated(EnumType.STRING)
	@Column
	private UserRole userRole;
	
	// list of users commentaries
	@OneToMany(mappedBy="user")
	protected Set<Commentary> setCommentaries = new HashSet<Commentary>();
		
	//personal gallery
	@OneToOne
	@JoinColumn
	private Gallery userGallery;
	
	// list of works for which has access right
	@ManyToMany(cascade=CascadeType.PERSIST, fetch=FetchType.EAGER)
	@JoinTable
	private Set<Work> setWorkUser = new HashSet<Work>();


	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@XmlIDREF
	public Gallery getUserGallery() {
		return userGallery;
	}
	public void setUserGallery(Gallery userGallery) {
		this.userGallery = userGallery;
	}

	public UserRole getUserRole() {
		return userRole;
	}
	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	@XmlIDREF
	public Set<Commentary> getSetCommentaries() {
		return setCommentaries;
	}
	public void setSetCommentaries(Set<Commentary> setCommentaries) {
		this.setCommentaries = setCommentaries;
	}
	
	@XmlIDREF
	public Set<Work> getSetWorkUser() {
		return setWorkUser;
	}
	public void setSetWorkUser(Set<Work> setWork) {
		this.setWorkUser = setWork;
	}
	
	@XmlID
	public String getStringId() {
		return ""+getId();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public User() {}

}
