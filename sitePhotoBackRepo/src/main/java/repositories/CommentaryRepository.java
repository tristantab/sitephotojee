package repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import models.Commentary;

@Repository
public interface CommentaryRepository extends JpaRepository<Commentary, Integer> {

	public List<Commentary> findByUser(String user);
	
	public List<Commentary> findByCommentContaining(String comment);

	
}
