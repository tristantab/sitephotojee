package repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import models.User;
import models.UserRole;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	
	List<User> findByLoginAndPassword(String login, String password);

	List<User> findByLastNameContaining(String lastName);

//	List<User> findByRole(UserRole userRole);
	
	
	@Query("select u from User u where u.firstName LIKE '%?1%'")
	List<User> findByFirstNameLike(String str);
	
}
