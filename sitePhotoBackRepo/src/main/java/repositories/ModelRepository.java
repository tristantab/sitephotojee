package repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import models.Model;
import models.ModelType;

@Repository
public interface ModelRepository extends JpaRepository<Model, Integer> {

	public List<Model> findByFirstNameOrLastNameContaining(String firstName, String lastName);
	
	public List<Model> findByModelType(ModelType modelType);
	
}
