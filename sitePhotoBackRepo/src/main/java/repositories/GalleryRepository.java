package repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import models.Gallery;


@Repository
public interface GalleryRepository extends JpaRepository<Gallery, Integer> {

	public List<Gallery> findByTitle(String title);
	
	public List<Gallery> findByTitleContaining(String title);
	
	public List<Gallery> findByUser(String user);
	
	public List<Gallery> findByDescriptionContaining(String description);
	
}
