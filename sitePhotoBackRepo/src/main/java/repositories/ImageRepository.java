package repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import models.Image;

@Repository
public interface ImageRepository extends JpaRepository<Image, Integer> {

	public List<Image> findByTitle(String title);
	
	public List<Image> findByTitleContaining(String title);
	
	public List<Image> findByLocationTaken(String locationTaken);
	
	public List<Image> findByTitleOrLocationTakenContaining(String title, String locationTaken);
	
	
	
}
