package dao;

import org.springframework.stereotype.Repository;

import models.Commentary;

@Repository
public class CommentaryDAO extends GenericDAO<Commentary>{

	public CommentaryDAO() {
		super(Commentary.class);
	}

}
