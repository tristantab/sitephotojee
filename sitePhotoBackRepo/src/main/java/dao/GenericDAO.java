package dao;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;


import org.hibernate.Session;
import org.hibernate.Transaction;

import exceptions.HibernateUsageException;




public class GenericDAO<T> {

	protected Session s;	
	
	private Class<T> clazz;
	
	
	public GenericDAO(Class<T> clazz) {
		this.clazz = clazz;
	}
	
	
	public Session getSession() {
		return s;
	}

	public void setSession(Session s) {
		this.s = s;
	}

	
	public T findById(int id) {
		T ret;
		ret = s.get(clazz , id);
		return ret;
	}

	public List<T> findAll() {
		List<T> ret;
		ret = (List<T>) s.createQuery("from " + clazz.getName()).list();
		return ret;
	}
	
	public boolean save(T a) throws HibernateUsageException {
		Transaction tx = null;
		try {
			tx = s.beginTransaction();
			s.persist(a); //merge
			tx.commit();
		} catch (EntityExistsException | IllegalArgumentException e) {
			if (tx != null)
				tx.rollback();
			throw new HibernateUsageException("Save failed", e);
//			return false;
		} catch (TransactionRequiredException | IllegalStateException | RollbackException e) {
			if (tx != null)
				tx.rollback();
			throw new HibernateUsageException("Save failed", e);
		}
		return true;
	}
	

	public boolean update(T a) throws HibernateUsageException {
		Transaction tx = null;
		try {
			tx = s.beginTransaction();
			s.update(a);
			tx.commit();
		} catch (EntityExistsException | IllegalArgumentException e) {
			if (tx != null)
				tx.rollback();
			throw new HibernateUsageException("Save failed", e);
//			return false;
		} catch (TransactionRequiredException | IllegalStateException | RollbackException e) {
			if (tx != null)
				tx.rollback();
			throw new HibernateUsageException("Save failed", e);
		}
		return true;
	}
	
	public void delete(T a) throws Exception {
		Transaction tx = null;
		try {
			tx = s.beginTransaction();
			s.delete(a);
			tx.commit();
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
	}
	
}
