package dao;

import models.Work;

public class WorkDAO extends GenericDAO<Work>{

	public WorkDAO() {
		super(Work.class);
	}

}
