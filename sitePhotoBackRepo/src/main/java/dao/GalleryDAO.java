package dao;

import org.springframework.stereotype.Repository;

import models.Gallery;

@Repository
public class GalleryDAO extends GenericDAO<Gallery>{

	public GalleryDAO() {
		super(Gallery.class);
	}

}
