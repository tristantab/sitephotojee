package dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import models.Model;

@Repository
public class ModelDAO extends GenericDAO<Model>{

	public ModelDAO() {
		super(Model.class);
	}
	
	public List<Model> findByName(String lastNameTry, String firstNameTry) {
		//LOOOOL
		List<Model> ret;
		Query q = s.createQuery("from Model where lastName like :lastname and firstName like :firstname");
		q.setParameter("lastname", "%"+lastNameTry+"%");
		q.setParameter("firstname", "%"+firstNameTry+"%");
		ret = q.getResultList();
		return ret;
	}
	

	public List<Model> findByFirstNameOrLastName(String pattern) {
		List<Model> ret;
		Query q = s.createQuery("from Model where lastName like :lastname or firstName like :firstname");
		q.setParameter("lastname", "%"+pattern+"%");
		q.setParameter("firstname", "%"+pattern+"%");
		ret = q.getResultList();
		return ret;
	}

}
