package dao;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import models.User;

@Repository
public class UserDAO extends GenericDAO<User>{

	public UserDAO() {
		super(User.class);	
	}
	
	public User findByLoginAndPassword(String login, String password) {
		User ret;
		Query q = s.createQuery("from User where login = :login and password = :password");
		q.setParameter("login", login);
		q.setParameter("password", password);
		try {
			ret = (User) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
		return ret;
	}
	
	public List<User> findByName(String lastNameTry, String firstNameTry) {
		List<User> ret;
		Query q = s.createQuery("from User where lastName like :lastname and firstName like :firstname");
		q.setParameter("lastname", "%"+lastNameTry+"%");
		q.setParameter("firstname", "%"+firstNameTry+"%");
		ret = q.getResultList();
		return ret;
	}
	

	public List<User> findByFirstNameOrLastName(String pattern) {
		List<User> ret;
		Query q = s.createQuery("from User where lastName like :lastname or firstName like :firstname");
		q.setParameter("lastname", "%"+pattern+"%");
		q.setParameter("firstname", "%"+pattern+"%");
		ret = q.getResultList();
		return ret;
	}

}
