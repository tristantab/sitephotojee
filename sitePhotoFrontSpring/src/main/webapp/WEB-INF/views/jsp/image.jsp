<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


	<div class="container "> 
		<div class="row d-flex justify-content-center">
		
			<div class="container imgPopupDiv d-flex justify-content-center"> 
<%-- 				<a href="" class="imgPopup" title="${imageChosen.title}" data-placement="left" data-toggle="popover"   data-content="description ${imageChosen.description}">infos</a>		 --%>
				
<!-- 				add btn back to gallery  -->
<!-- 	add btns back & forward within gallery -->
				<span class="imgPopup" data-toggle="popover" data-trigger="hover" data-placement="bottom" title="${imageChosen.title}"
					data-content="${imageChosen.description} ${imageChosen.locationTaken} ${imageChosen.dateTaken}">
				<i class="fas fa-info-circle"></i> INFOS
				</span>
			</div>
			
			<div class="imgDisplayFullDiv d-flex justify-content-center" id="${imageChosen.id}">
				<img src="${pageContext.request.contextPath}/${imageChosen.urlLocation}" alt="${imageChosen.title}" class=" mw-100 mh-100 .img-fluid"/>
			</div>
		
			<div>


<!-- 				<ul class="model-list col"> -->
<%-- 					<c:forEach items="${imageChosen.setCommentaries}" var="commentary"> --%>
<%-- 						<li class=" col-md-3 user justify-content-between" id="${commentary.id}" data-content="user" data-id="${commentary.id}"> --%>
<!-- 							<div> -->
<%-- 								<p class="h4"><i class="far fa-comment"></i><i>  by ${commentary.user}</i></p> --%>
<%-- 								<p>${commentary.comment}</p> --%>
<!-- 							</div> -->
<%-- 							<c:if test="${connectedUser.userRole == 'admin'}"> --%>
<!-- 							<div> -->
<!-- 								<div class="btn-user-remove user-list-btn m-1"> -->
<%-- 									<button id="user-${user.id}" class="btn btn-danger" onclick="clickEditUserButton(this)" data-id="${user.id}">Edit</button> --%>
<!-- 								</div> -->
<!-- 								<div class="btn-user-remove user-list-btn m-1"> -->
<%-- 									<button id="user-${user.id}" class="admin rm-user-button btn btn-danger" onclick="clickRemoveUserButton(this)" data-id="${user.id}">Delete</button> --%>
<!-- 								</div> -->
<!-- 							</div> -->
<%-- 							</c:if>				 --%>
<!-- 						</li> -->
<%-- 					</c:forEach> --%>
<!-- 				</ul> -->



			</div>
			
		</div>
	</div>
