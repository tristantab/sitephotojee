<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<ul class="model-list">
<!-- <ul class="user-list row"> -->
	<c:forEach items="${models}" var="model">
	<li class="user element" id="${model.id}" data-login="${model.lastName}" data-content="user" data-id="${model.id}"
		data-searchable="${model.id} ${model.firstName} ${model.lastName} ${model.modelType}">
<%-- 		<li class="col-sm-4 user" id="${model.id}" data-login="${model.lastName}" data-content="user" data-id="${model.id}"> --%>
			<div>
				<p class="h3">${model.firstName} ${model.lastName}</p>
				<i>(${model.modelType})</i>
			</div>
			<div class="">
				<h6 class="txtToCenter">featured in :</h6>
				<div class="container mt-4">
					<div class="row d-flex justify-content-center">
						<c:forEach items="${model.setWorkMod}" var="work">
							<div class="col-auto mb-4">
<!-- 							col-auto -->
								<div class="card"  onclick="location.href='${pageContext.request.contextPath}/image/?id=${work.id}'">
									<div class="cardImageDiv d-flex justify-content-center">
										<img class="card-img-top cardContent mw-100" src="${pageContext.request.contextPath}/${work.urlLocation}" alt="${work.title} image"/>
									</div>	
									<div class="card-body">
										<h4 class="card-title cardContent">${work.title}</h4>
										<p class="card-text cardContent">${work.description}</p>
								  	</div>
								</div>
							</div>
						</c:forEach>
					</div>
				</div>
			</div>
			<c:if test="${connectedUser.userRole == 'admin'}">
				<div>
					<div class="btn-user-remove user-list-btn m-1">
						<button id="user-${user.id}" class="btn btn-danger" onclick="clickEditModelButton(this)" data-id="${user.id}">Edit</button>
					</div>
					<div class="btn-user-remove user-list-btn m-1">
						<button id="user-${user.id}" class="admin rm-user-button btn btn-danger" onclick="clickRemoveModelButton(this)" data-id="${user.id}">Delete</button>
					</div>
				</div>
			</c:if>				
		</li>
	</c:forEach>
 </ul>