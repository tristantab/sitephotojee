<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="container mt-4">
	<div class="">

		<ul class="user-list row justify-content-between">
			<c:forEach items="${users}" var="user">
				<li class="element col-md-3 user justify-content-between" id="${user.id}" data-login="${user.login}" data-content="user" data-id="${user.id}"
				data-searchable="${user.login} ${user.firstName} ${user.lastName} ${user.userRole}">
					<div>
						<p class="h4">${user.login}</p>
						<p>${user.firstName} ${user.lastName}
							<c:if test="${user.userRole == 'admin'}">
								<i> (administrator)</i>
							</c:if>
							<c:if test="${user.userRole == 'simpleUser'}">
								<i> (regular user)</i>
							</c:if>
						</p>
					</div>
					<c:if test="${connectedUser.userRole == 'admin'}">
					<div>
						<div class="btn-user-remove user-list-btn m-1">
							<button id="user-${user.id}" class="btn btn-danger" onclick="clickEditUserButton(this)" data-id="${user.id}">Edit</button>
						</div>
						<div class="btn-user-remove user-list-btn m-1">
							<button id="user-${user.id}" class="admin rm-user-button btn btn-danger" onclick="clickRemoveUserButton(this)" data-id="${user.id}">Delete</button>
						</div>
					</div>
					</c:if>				
				</li>
			</c:forEach>
		</ul>
	
	</div>
</div>