<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- 	Regular display version -->

<%-- <c:forEach items="${galleries}" var="gallerie"> --%>
<%-- 	<div class="row media" id="${gallerie.id}"> --%>
<!-- 		<div class="column-25 media-column"> -->
<%-- 			<img src="${gallerie.imageRef.urlLocation}" height="200px" --%>
<%-- 				class='link' onclick="location.href='./${gallerie.id}'" /> --%>
<%-- 				<h3>${gallerie.imageRef.urlLocation}</h3> --%>
<!-- 		</div> -->
<!-- 		<div class="column-75 media-column"> -->
<%-- 			<h4>${gallerie.title}</h4> --%>

<%-- 			<p>${gallerie.description}</p> --%>

<!-- 		</div> -->
<!-- 	</div> -->
<%-- </c:forEach> --%>


<!-- 	Carousel version -->

<!-- https://github.com/Jahia/bootstrap3/blob/master/bootstrap3-components/src/main/resources/bootstrap3nt_carousel/html/carousel.jsp -->

<div class="container mt-4">
	<div class="row justify-content-center">

		<div id="carouselIndicators" class="carousel slide imgCarouselDiv"
			data-ride="carousel">

			<ol class="carousel-indicators">
				<c:forEach items="${galleries}" var="gallerie" varStatus="status">
					<li data-target="#carouselIndicators"
						data-slide-to="${status.index}"
						class="<c:if test='${status.first}'>active</c:if>"></li>
				</c:forEach>
			</ol>


			<div class="carousel-inner ">

				<c:forEach items="${galleries}" var="gallerie" varStatus="status">
					<div class="carousel-item <c:if test='${status.first}'>active</c:if>" data-searchable="${gallerie.id} ${gallerie.title} ${gallerie.description}">
						<div class="d-flex justify-content-center imgCarouselDiv">
							<img class="mw-100 mh-100" src="${gallerie.imageRef.urlLocation}"
								alt="${gallerie.title}"
								onclick="location.href='./gallery/?id=${gallerie.id}'">
						</div>
						<div class="carousel-caption d-none d-md-block">
							<h3>---</h3>
							<h3>${gallerie.title}</h3>
							<h5>${gallerie.description}</h5>
					</div>
					</div>
				</c:forEach>

			</div>


			<a class="carousel-control-prev" href="#carouselIndicators"
				role="button" data-slide="prev"> <span
				class="carousel-control-prev-icon" aria-hidden="true"></span>
			</a> <a class="carousel-control-next" href="#carouselIndicators"
				role="button" data-slide="next"> <span
				class="carousel-control-next-icon" aria-hidden="true"></span>
			</a>

		</div>

	</div>
</div>




