<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="container mt-4">


<!-- 			<ul class="user-list row justify-content-between"> -->
			<ul class="model-list col ">
				<c:forEach items="${commentaries}" var="commentary">
					<li class="element user justify-content-between" id="${commentary.id}" data-content="user" data-id="${commentary.id}"
					data-searchable="${commentary.user} ${commentary.comment}">
						<div>
							<p class="h4"><i class="far fa-comment"></i><i>  by ${commentary.user.login}</i></p>
							<p>${commentary.comment}</p>
						</div>
						<c:if test="${connectedUser.userRole == 'admin'}">
						<div>
							<div class="btn-user-remove user-list-btn m-1">
								<button id="user-${user.id}" class="btn btn-danger" onclick="clickEditCommentaryButton(this)" data-id="${user.id}">Edit</button>
							</div>
							<div class="btn-user-remove user-list-btn m-1">
								<button id="user-${user.id}" class="admin rm-user-button btn btn-danger" onclick="clickRemoveCommentaryButton(this)" data-id="${user.id}">Delete</button>
							</div>
						</div>
						</c:if>				
					</li>
				</c:forEach>
			</ul>
	

</div>