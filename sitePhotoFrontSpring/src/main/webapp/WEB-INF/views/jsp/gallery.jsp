<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>






	<!-- Card version -->

<div class="container mt-4">
<div class="row justify-content-center">

	<c:forEach items="${galleryChosen.setWorkGal}" var="work">
		<div class="element col-auto mb-4" data-searchable="${work.id} ${work.title} ${work.description}">
			<div class="card"  onclick="location.href='${pageContext.request.contextPath}/image/?id=${work.id}'">
				<div class="cardImageDiv d-flex justify-content-center">
					<img class="card-img-top cardContent mw-100" src="${pageContext.request.contextPath}/${work.urlLocation}" alt="${work.title} image"/>
				</div>	
				<div class="card-body">
					<h4 class="card-title cardContent">${work.title}</h4>
					<p class="card-text cardContent">${work.description}</p>
			  	</div>
			</div>
		</div>
	</c:forEach>

</div>
</div>
