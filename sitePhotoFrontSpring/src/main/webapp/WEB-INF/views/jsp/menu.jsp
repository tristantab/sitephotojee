<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>



<nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark" style="background-color: #160b11;">

	<c:if test="${empty connectedUser}">
<!-- 		<a class="navbar-brand" href="#">Welcome</a> -->
		<span class="navbar-brand" >Welcome</span>
	</c:if>
	<c:if test="${not empty connectedUser}">		
		<a class="navbar-brand" href="#"><i class="fas fa-user-circle"></i> ${connectedUser.login}</a>
	</c:if>

<!-- 	Responsive collapse plus menu button -->
	<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    	<span class="navbar-toggler-icon"></span>
  	</button>

	<div class="collapse navbar-collapse" id="collapsibleNavbar">
		<ul class="navbar-nav mr-auto">

<!-- 		ADD HIGHLIGHT WHEN WE ARE HERE -->
			<li class="nav-item active"><a class="nav-link"
				href="/sitePhotoFrontSpring/home">Home</a></li>

<!-- 	Galleries Dropdown -->

			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Galleries </a>				
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<c:forEach items="${galleries}" var="galleryNav">
						<a class="dropdown-item" href="${pageContext.request.contextPath}/gallery/?id=${galleryNav.id}">${galleryNav.title}</a>
					</c:forEach>
				</div>
			</li>


			<c:if test="${not empty connectedUser}">
<!-- 				<li class="nav-item" -->
<%-- 					onclick="window.location.href='/${user.id}/mygallery'"><a --%>
<!-- 					class="nav-link" href="#">MyGallery</a></li> -->
											
				
<!-- 					<div class="btn-group" role="group"> -->


<!-- 	Utils Dropdown -->
					
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
						role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Utils </a>				
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<button class="dropdown-item"
								onclick="window.location.href='/sitePhotoFrontSpring/users'">
								<i class="fas fa-address-book"></i> Users List
							</button>
							<button class="dropdown-item"
								onclick="window.location.href='/sitePhotoFrontSpring/models'">
								<i class="fas fa-address-book"></i> Models List
							</button>
							<button class="dropdown-item"
								onclick="window.location.href='/sitePhotoFrontSpring/commentaries'">
								<i class="fas fa-address-book"></i> Commentaries List
							</button>
							
<%-- 							<c:if test="${connectedUser.userRole == 'admin'}"> --%>
<!-- 								<div class="dropdown-divider"></div> -->
<%-- 			<%-- 				<c:if test="window.location.href.isEqual('/home')">  --%>
<!-- 								<button class="dropdown-item" -->
<!-- 									onclick="window.location.href='/sitePhotoFrontSpring/login'"> -->
<!-- 									<i class="fas fa-plus-circle"></i> Add Gallery -->
<!-- 								</button> -->
<!-- 								<button class="dropdown-item" -->
<!-- 									onclick="window.location.href='/sitePhotoFrontSpring/login'"> -->
<!-- 									<i class="fas fa-minus-square"></i> Delete Gallery -->
<!-- 								</button> -->
<%-- 			<%-- 				</c:if> --%>
<%-- 											<c:if test="window.location.href is in gallery">  --%>
<!-- 								<div class="dropdown-divider"></div> -->
<!-- 								<button class="dropdown-item" -->
<!-- 									onclick="window.location.href='/sitePhotoFrontSpring/login'"> -->
<!-- 									<i class="fas fa-plus-circle"></i> Add Image -->
<!-- 								</button> -->
<!-- 								<button class="dropdown-item" -->
<!-- 									onclick="window.location.href='/sitePhotoFrontSpring/login'"> -->
<!-- 									<i class="fas fa-minus-square"></i> Delete Image -->
<!-- 								</button> -->
<%-- 			<%-- 				</c:if> --%>
							
								
<%-- 							</c:if>	 --%>
							
							
						
						</div>
					</li>
						
<!-- 					</div>									 -->
				
				
			</c:if>
			
		</ul>
		
<%-- 		<c:if test="${pageContext.request.requestURI.endsWith('/home')}"> --%>
<%-- 		<c:if test="${fn:contains(pageContext.request.contextPath, 'home')}"> --%>
			<c:if test="${searchEnable == true}">
			<form class="form-inline my-2 my-lg-0 mr-2">			
				<input class="form-control mr-sm-2" type="text" id="search" placeholder="search images..." onkeyup="keyupSearchInput(this)" aria-label="Search">
				<div class="input-group-prepend mr-2">
				        <span class=""><i class="fa fa-search fa-lg"></i></span>
				</div>
			</form>

		</c:if>
		
		<c:if test="${empty connectedUser}">			
			<button class="btn navbar-btn btn-dark" id="add-user-modal-button" onclick="clickAddUserButton(this)"><i class="fas fa-user-plus"></i> Sign up</button>
			<button class="btn navbar-btn btn-dark" id="show-signin-modal-button" data-toggle="modal" data-target="#signin-modal"><i class="fas fa-sign-in-alt"></i> Sign in</button>
		</c:if>
		<c:if test="${not empty connectedUser}">
			<button class="btn navbar-btn btn-dark" id="add-user-modal-button" onclick="clickEditUserButton(this)" data-id="${connectedUser.id}"><i class="fas fa-user-edit"></i> Edit User</button>
			<button class="btn navbar-btn btn-dark" onclick="clickSignOutButton()"><i class="fas fa-sign-out-alt"></i> Sign out</button>
		</c:if>

	</div>

</nav>


<!-- SIGN IN MODAL -->

<div id="signin-modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Login form</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body row">
				<div class="col-sm-offset-2 col-sm-8">
					<form class="form-horizontal" action="" method="post">
						<div class="input-group">
							<span class="input-group-addon"> <i	class="glyphicon glyphicon-user"></i>
							</span> <input id="login" type="text" name="login" class="form-control"
								placeholder="Login" autofocus>
						</div>
						<div class="input-group">
							<span class="input-group-addon"> <i class="glyphicon glyphicon-lock"></i>
							</span> <input id="password" type="password" name="password"
								class="form-control" placeholder="Password">
						</div>
<%-- 						<input type="hidden" name="_csrf" value="${_csrf.token}" /> --%>
						<div class="modal-footer">
							<input type="button" id="login-submit-button" class="btn btn-default" value="Sign in">
						</div>
					</form>
					<div class="alert alert-danger" style="display: none;"></div>
				</div>
			</div>

		</div>
	</div>
</div>


<!-- MESSAGE MODAL -->

<div id="showMessage-modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Message</h4>
			</div>
			<div class="modal-body">
				<div class="alert alert-success">
					User successfully removed !
				</div>
			</div>
		</div>
	</div>
</div>


