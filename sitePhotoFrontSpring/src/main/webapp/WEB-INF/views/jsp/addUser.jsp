<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- SIGN UP MODAL -->

<div class="modal fade" id="add-user-modal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header text-center">
				<h4 class="modal-title w-100 font-weight-bold">
					<span class="glyphicon glyphicon-lock"></span> User Account
				</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body mx-3">

				<form class="form-horizontal" method="post" action="edit">
<!-- 				signup -->
					<div class="md-form mb-5">
						<i class="fa fa-user prefix grey-text"></i> <label data-error="wrong" data-success="right" for="orangeForm-name">Login</label>
						<input type="text" name="login" id="orangeForm-name" class="form-control validate" value="${newUser.login}" autofocus/>
<!-- 						onkeyup="keyupInputUserLogin(this)" -->
					</div>

					<div class="md-form mb-4">
						<i class="fa fa-lock prefix grey-text"></i> <label data-error="wrong" data-success="right" for="orangeForm-pass">Password</label>
						<input type="password" name="password" id="orangeForm-pass" class="form-control validate" value="${newUser.password}" />
					</div>
					
					<div class="md-form mb-4">
						<i class="fa fa-lock prefix grey-text"></i> <label data-error="wrong" data-success="right" for="orangeForm-pass">Your role</label><br>
						<c:if test="${not empty newUser && (newUser.id != 0)}">
							<select name="userRole" disabled>							
								<option value="${newUser.userRole}" selected>${newUser.userRole}</option>
							</select>
						</c:if>
						<c:if test="${(empty newUser) || (newUser.id == 0)}">
							<select name="userRole">
								<option value="admin">Admin</option>
								<option value="simpleUser" selected>Regular User</option>
							</select>
						</c:if>
					</div>

					<div class="md-form mb-4">
						<i class="fa fa-lock prefix grey-text"></i> <label data-error="wrong" data-success="right" for="orangeForm-firstName">Firstname</label>
						<input type="text" name="firstName" id="orangeForm-firstName" class="form-control validate" value="${newUser.firstName}" />
					</div>

					<div class="md-form mb-4">
						<i class="fa fa-lock prefix grey-text"></i> <label data-error="wrong" data-success="right" for="orangeForm-lastName">Lastname</label>
						<input type="text" name="lastName" id="orangeForm-lastName" class="form-control validate" value="${newUser.lastName}"/>
					</div>
				</form>
				<div class="alert alert-danger" style="display: none;"></div>

			</div>


			<div class="modal-footer d-flex justify-content-center">
				<button class="btn btn-deep-orange" id="submit-user-button" onclick="clickSubmitUserButton(this)" data-id="${newUser.id}">Save</button>
			</div>
		</div>
	</div>
</div>