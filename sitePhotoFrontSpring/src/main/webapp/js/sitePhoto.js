//$(document).ready(function() {
//
//
//	// Variables ///////////////////////////////////////////////////////////////////////////////////
//
//	// Object to store the number of product selected per media
//	var produits = new Object();
//	// Variable to store the total number of media selected by the user
//	var nbProduits = 0;
//	// Variable to store the current page
//	var page;
//	
//	// Login / logout management //////////////////////////////////////////////////////////////////
//	
//	// event listener for login button
//	$("#login-button").click(function() {
//		$.ajax({
//				"url": "login",
//				"data": {
//					"login": $("#loginModal input[name=login]").val(), 
//					"password": $("#loginModal input[name=password]").val(),
//					"url": location.href
//					},
//				"method": "POST",
//				"error": function(xhr, status, error) {
//						$("#loginModal .alert").removeClass("alert-success").addClass("alert-danger").html("<strong>Error: </strong>"+xhr.responseText).show();
//					},
//				"success": function(data, status, xhr) {
//						$("#loginModal .alert").removeClass("alert-danger").addClass("alert-success").html("You are succesfully logged !").show();
//						window.setTimeout(function() {
//							location.href = "/BibliothequeFront-Spring";
//						}, 1000);
//						}
//			});
//	});
//	
//	// event listener for logout button
//	$("#logout-button").click(function() {
//		$.ajax(
//				"logout",
//				{
//					"success": function(data, status, xhr) {
//						location.href = "/BibliothequeFrontAjax";
//					}
//				}
//			);
//	});
//	
//	
//	
//	// Add/edit Media management /////////////////////////////////////////////////////////////////
//	
//	// event listener for add media button (form submission)
//	$("#add-media-button").click(function() {
//		$.ajax(
//				"media", 
//				{
//					"data": {
//						"title": $("#addMediaModal input[name=title]").val(), 
//						"authors": $("#addMediaModal select[name=authors]").val(), 
//						"abstract": $("#addMediaModal textarea[name=abstract]").val(), 
//						"cover": $("#addMediaModal input[name=cover]").val(), 
//						"url": location.href
//						},
//					"method": "POST",
//					"error": function(xhr, status, error) {
//								$("#addMediaModal .alert").removeClass("alert-success").addClass("alert-danger").html("<strong>Error: </strong>"+xhr.responseText).show();
//							},
//					"success": function(data, status, xhr) {
//								$("#addMediaModal .alert").removeClass("alert-danger").addClass("alert-success").html("Media succesfully added !").show();
//								window.setTimeout(function() {
//									$("#addMediaModal").modal("hide");
//									$("#addMediaModal input[name=title]").val("");
//									$("#addMediaModal select[name=authors]").val("");
//									$("#addMediaModal select[name=authors]:not(:first-child)").remove();
//									$("#addMediaModal textarea[name=abstract]").val("");
//									$("#addMediaModal input[name=cover]").val("");
//									$("#addMediaModal .alert").hide();
//									$("#media-list-location").append(xhr.responseText);
//								}, 1000);
//							}
//			});
//	});
//	
//	// Authors selection in media form: save old value when the focus is on an author select
//	$("#authors-container select").focusin(function() {
//		$(this).data("oldValue", $(this).val());
//	});
//	
//	// Authors selection in media form: if the value of an author select change and the old value is the defaults one then append a new author select and add a delete button to the current one
//	$("#authors-container select").change(function() {
//		if ($(this).data("oldValue") == null) {
//			// clone the parent of the current select
//			var e = $(this).parent().parent().parent().clone("true");
//			// append the new element
//			$(this).parents().filter("#authors-container").append(e);
//			// show the remove button of the current select
//			$(this).next().show();
//			// resize the current element to make room for the button
//			// $(this).css({"width":"90%"});
//			// append a new delete button to the parent of the current element
//			// $(this).parent().append("<input type='button' class='remove-author' style='float:right;' value='X'></input>");
//			// adding listener on button
//			addAuthorSelectListener();
//		}
//		$(this).data("oldValue", $(this).val());
//	});
//	
//	// Authors selection in media form: add event listener which remove an author select when the remove button is clicked
//	function addAuthorSelectListener() {
//		$(".remove-author").click(function(){
//			console.log("removing author select");
//			$(this).parent().parent().parent().parent().remove();
//		});
//	}
//	
//	addAuthorSelectListener();
//	
//	
//	
//	// Add / edit author management //////////////////////////////////////////////////////////////
//
//	$("#show-edit-author-modal").click(function() {
//		$.ajax({
//			url: "editauthor",
//			method: "GET",
//			success: function(data, status, xhr) {
//					$("#addAuthorModal .modal-content").html(xhr.responseText);
//					addEditAuthorSubmitButtonListener();
//				}
//			});
//	});
//
//	function addEditAuthorSubmitButtonListener() {
//		$("#add-author-button").click(function() {
//			$.ajax(
//					"author", 
//					{
//						"data": {
//							"firstname": $("#addAuthorModal input[name=firstname]").val(), 
//							"lastname": $("#addAuthorModal input[name=lastname]").val(),
//							"url": location.href
//							},
//						"method": "POST",
//						"error": function(xhr, status, error) {
//									$("#addAuthorModal .alert").addClass("alert-danger").html("<strong>Error: </strong>"+xhr.responseText).show();
//								},
//						"success": function(data, status, xhr) {
//									$("#addAuthorModal .alert").removeClass("alert-danger").addClass("alert-success").html("Author succesfully added !").show();
//									window.setTimeout(function() {
//										$("#addAuthorModal").modal("hide");
//										$("#addAuthorModal input[name=firstname]").val("");
//										$("#addAuthorModal input[name=lastname]").val("");
//										$("#author-list-location").append(xhr.responseText);
//									}, 1000);
//								}
//				});
//		});
//	}
//	
//	
//	// Authors management /////////////////////////////////////////////////////////////////////////
//	
//	$(".button-remove-author").click(function() {
//		// getting author id
//		var id = $(this).data("id");
//		// ajax query
//		$.get("removeauthor?id="+id, function() {
//			// on succes: removing author div from DOM
//			$("#"+id).remove();
//		});
//	});
//	
//	
//	// Media management ///////////////////////////////////////////////////////////////////////////
//	
//	$(".button-remove-media").click(function() {
//		// getting media id
//		var id = $(this).data("id");
//		// ajax query
//		$.get("removemedia?id="+id, function() {
//			// on success: remove media div from DOM
//			$("#"+id).remove();
//		});
//	});
//	
//	
//	
//	// Search management (Ajax)  //////////////////////////////////////////////////////////////////
//	
//	
//	// add listener on the search field to update the media list
//	$("#search").keyup(function() {
//		switch (page) {
//		case "authors" :
//			loadAuthors();
//			break;
//		case "books" :
//			loadBooks();
//			break;
//		}
//	});
//
//	// function executing the ajax query to retrieve the list of books and inserting the result in the page
//	function loadBooks() {
//		var pattern = $("#search").val().toLowerCase();
//		$(".books").show().filter(function() {
//			$(this).toggle($(this).data("title").toLowerCase().indexOf(pattern) > -1);
//		});
//		$(".authors").hide();
//	}
//
//	// function executing the ajax query to retrieve the list of authors and inserting the result in the page
//	function loadAuthors() {
//		var pattern = $("#search").val().toLowerCase();
//		$(".authors").show().filter(function() {
//			$(this).toggle($(this).data("name").toLowerCase().indexOf(pattern) > -1);
//		});
//		$(".books").hide();
//	}
//	
//
//	// Page management ////////////////////////////////////////////////////////////////////////////
//	
//	// on loading: show book list page
//	showBooks();
//	
//	// add listener on the button to switch between author list page and book list page
//	$("#authors-button").click(showAuthors);
//	$("#books-button").click(showBooks);
//	
//	// function to switch to author list page
//	function showAuthors() {
//		// setting variable
//		page = "authors";
//		// updating button styles
//		$(this).addClass("btn-primary");
//		$("#books-button").removeClass("btn-primary");
//		// loading content
//		loadAuthors();
//	}
//
//	// function to switch to book list page
//	function showBooks() {
//		// setting variable
//		page = "books";
//		// updating button styles
//		$(this).addClass("btn-primary");
//		$("#authors-button").removeClass("btn-primary");
//		// loading content
//		loadBooks();
//	}
//	
//
//
//
//	// Media selection managment ///////////////////////////////////////////////////////////////////
//	
//	// Function adding listeners to the media selection related inputs
//	function addOrderListeners() {
//		// adding listeners on the + and - buttons of the products
//		$(".p-button,.m-button").click(function(){
//			var pid = $(this).attr("id").substring(0, $(this).attr("id").length-2);
//			produits[pid] += ($(this).text() == "+") ? 1 : -1;
//			nbProduits += ($(this).text() == "+") ? 1 : -1;
//			updateMediaVisibility(pid);
//			$("#"+pid+"-i").val(produits[pid]);
//		});
//	
//		// adding listeners on the media inputs
//		$(".media-input").change(function() {
//			var id = $(this).attr("id").split("-")[0];
//			var val = parseInt($(this).val());
//			nbProduits -= produits[id];
//			produits[id] = Math.max(isNaN(val) ? 0 : val, 0);
//			nbProduits += produits[id];
//			updateMediaVisibility(id);
//		});
//
//	}
//
//	
//	//Function adding listeners to the gallery selection related inputs
//	

	

// Document.ready Starts ///////////////

$(document).ready(function(){	

	// searchBarToggleDiv shows sur /gallery, /users, /models
	
//	function searchBarToggleDivFctn() {
//		
//	}

	//event listener for login validation button	
	$("#login-submit-button").click(function(){
		$.ajax(
				"/sitePhotoFrontSpring/login", 
				{
					"data": {		
						"login": $("#signin-modal input[name=login]").val(),
						"password":$("#signin-modal input[name=password]").val(),
						"url": location.href
					},
					"method":"POST",
					"error": function(xhr, status,error) {
						$("#alert").addClass("alert-danger").html("Error : "+xhr.responseText).show();
					},
					"success": function(data, status, xhr){
						$("#signin-modal .alert").removeClass("alert-danger").addClass("alert-success").html("You are successfully logged !").show();
						window.setTimeout(function() {
							location.href = location.href;
						}, 1000);
						
					}
				}
			)
		});
	

});

// Document.ready Ends ///////////////


// LOGIN MANAGEMENT /////////////////////////////////

// Sign out

function clickSignOutButton() {
	$.ajax({
			"url": "/sitePhotoFrontSpring/logout",
			"method": "GET",
			"success": function(data, status, xhr) {
					$("#content-location").html(xhr.responseText);
					$("#signin-modal .alert").removeClass("alert-danger").addClass("alert-success").html("You have logged out!").show();
					window.setTimeout(function() {
						location.href = "/sitePhotoFrontSpring/home";
					}, 1000);
					
				}
		});
}

//USERS MANAGEMENT /////////////////////////////////


//Delete user trigger-button

function clickRemoveUserButton(rub) {
	var id = $(rub).data("id");
	$.ajax({
		"url": "/sitePhotoFrontSpring/users/rm", 
		"data": {
				"id": id,
			},
		"method": "GET",
		"error": function(xhr, status, error) {
				//showMessage("<strong>Error: </strong>"+xhr.responseText, "alert-danger");
			},
		"success": function(data, status, xhr) {
				//showMessage("User succesfully removed !", "alert-success" ,1000);
				//$(".user[data-id="+id+"]").remove();
			
				$("#showMessage-modal").modal("show");
				//$("#showMessage .alert").removeClass("alert-danger").addClass("alert-success").html("User successfully removed !").show();								
				window.setTimeout(function() {
					location.href = "/sitePhotoFrontSpring/users";
				}, 1000);				
				$("#showMessage-modal").modal("hide");
				
			}
		});
}

// Add user trigger-button

function clickAddUserButton() {
	$.ajax({
		"url": "/sitePhotoFrontSpring/users/add",
		"method": "GET",
		"error": function(xhr, status, error) {
				showMessage("<strong>Error: </strong>"+xhr.responseText, "alert-danger");
			},
		"success": function(data, status, xhr) {
				$("#add-user-modal-content").html(xhr.responseText);
				$("#add-user-modal").modal("show");
			}
	});
}




// Edit user trigger-button

function clickEditUserButton(eub) {
	var id = $(eub).data("id");
	$.ajax({
		"url": "/sitePhotoFrontSpring/users/edit",
		"method": "GET",
		"data": {
				"id": id,
			},
		"error": function(xhr, status, error) {
				showMessage("<strong>Error: </strong>"+xhr.responseText, "alert-danger");
			},
		"success": function(data, status, xhr) {
				$("#add-user-modal-content").html(xhr.responseText);
				$("#add-user-modal").modal("show");
			}
	});
}


// Signup & edit user account submit trigger-button 

function clickSubmitUserButton(sub) {
	var id = $(sub).data("id");
	$.ajax({
		"url": "/sitePhotoFrontSpring/users/add",
		"method": "POST",
		"data": {
				"id": id,
				"login": $("#add-user-modal input[name=login]").val(),
				"password":$("#add-user-modal input[name=password]").val(),
				"userRole":$("#add-user-modal select[name=userRole]").val(),
				"firstName":$("#add-user-modal input[name=firstName]").val(),
				"lastName":$("#add-user-modal input[name=lastName]").val()
			},
		"error": function(xhr, status, error) {
				$("#add-user-modal .alert").addClass("alert-danger").html("<strong>Error: </strong>"+xhr.responseText).show();
			},
		"success": function(data, status, xhr) {
				$("#add-user-modal .alert").removeClass("alert-danger").addClass("alert-success").html("Utilisateur successfully edited !").show();
				window.setTimeout(function() {
					$("#add-user-modal").modal("hide");
					$("#user-list-location").append(xhr.responseText);
				}, 1000);
			}
	});
}
	

// MODELS MANAGEMENT /////////////////////////////////



//Search in Navbar /////////////////////////////////

function keyupSearchInput(si) {
	var pattern = $(si).val().toLowerCase();
	$(".element").show().filter(function() {
		$(this).toggle($(this).data("searchable").toLowerCase().indexOf(pattern) > -1);
	});

}



// GENERAL HTML /////////////////////////////////////

// Enable popovers on whole page

$(function () {
	$('[data-toggle="popover"]').popover();
})
	





	

	


