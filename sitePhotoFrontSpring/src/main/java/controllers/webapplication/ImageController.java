package controllers.webapplication;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import models.Gallery;
import models.Image;
import repositories.GalleryRepository;
import repositories.ImageRepository;

//import validators.LivreValidator;

@Controller
@RequestMapping("/image")
public class ImageController {

	@Autowired
	private ImageRepository imageRepository;
	
	@Autowired
	private GalleryRepository galleryRepository;
	

	@GetMapping 
	public String ImagesGetById(@RequestParam("id") int id, Model model) {
		System.out.println(id);
		// galleries displayed in navbar dropdown-menu
		model.addAttribute("galleries", galleryRepository.findAll());
		
		boolean searchEnable = false;
		model.addAttribute("searchEnable", searchEnable);
		
		Optional<Image> opt = imageRepository.findById(id);
		if (!opt.isPresent())
			return "no way Jose";
//			return new ResponseEntity<String>("No book found with id "+id, HttpStatus.BAD_REQUEST);
		else {
			model.addAttribute("imageChosen", opt.get());		
		}
		return "image";
	}

}
