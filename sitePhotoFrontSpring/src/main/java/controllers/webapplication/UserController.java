package controllers.webapplication;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import utils.ViewRenderer;
import models.UserRole;
import models.User;
import repositories.GalleryRepository;
import repositories.UserRepository;

@Controller
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserRepository userRepository;
	

	@Autowired
	private ViewRenderer viewRenderer;

	@Autowired
	private GalleryRepository galleryRepository;

	@GetMapping
	public String userGet(Model model) throws Exception {
		
		// galleries displayed in navbar dropdown-menu
		model.addAttribute("galleries", galleryRepository.findAll());
		
		boolean searchEnable = true;
		model.addAttribute("searchEnable", searchEnable);
				
		model.addAttribute("users", userRepository.findAll());
		return "users";
	}
		
	@GetMapping("/add")
	@ResponseBody
	public ResponseEntity<String> addUserGet(Model model, HttpServletRequest request, HttpServletResponse response,
			Locale locale) throws Exception {
		model.addAttribute("newUser", new User());
		return new ResponseEntity<String>(viewRenderer.render("addUser", locale, model, request), HttpStatus.OK);
	}

	@PostMapping("/add")
	@ResponseBody
	public ResponseEntity<String> addUserPost(@Valid @ModelAttribute("newUser") User user, BindingResult br,
			Model model, HttpServletRequest request, HttpServletResponse response, Locale locale) throws Exception {
		if (br.hasErrors()) {
			String message = "Invalid add user form";
			for (ObjectError e : br.getAllErrors())
				message += e.toString();
			return new ResponseEntity<String>(message, HttpStatus.BAD_REQUEST);
		}
//		User sessionUser = (User) request.getSession(false).getAttribute("user");
//		if (sessionUser != null) {
//			if (sessionUser.hasRole(RoleUtilisateur.ROLE_Admin)) {
//				user.setValidated(true);
//			}
//		} else {
//			user.setValidated(false);
//		}
//		user = userService.save(user);				
//		model.addAttribute("users", Arrays.asList(user));
		
		List<User> results = userRepository.findByLoginAndPassword(user.getLogin(), user.getPassword());
		if (results.size() > 0) {
			return new ResponseEntity<String>("User LOGIN & PASSWORD already exist !", HttpStatus.BAD_REQUEST);
		} else {
			userRepository.save(user);
			return new ResponseEntity<String>(HttpStatus.OK);
		}
		

	}
	
	@GetMapping("/edit")
	@ResponseBody
	public ResponseEntity<String> editUserGet(@RequestParam("id") int id, Model model, HttpServletRequest request, HttpServletResponse response, Locale locale) throws Exception {
		Optional<User> u = userRepository.findById(id);
		if (!u.isPresent())
			return new ResponseEntity<String>("No user found with id "+id, HttpStatus.BAD_REQUEST);
		model.addAttribute("newUser", u.get());
		return new ResponseEntity<String>(viewRenderer.render("addUser", locale, model, request), HttpStatus.OK);
	}

	@GetMapping("/rm")
	@ResponseBody
	public ResponseEntity<String> rmUser(@RequestParam("id") int id) {
		userRepository.deleteById(id);
		return new ResponseEntity<String>(HttpStatus.OK);
	}


	
}
