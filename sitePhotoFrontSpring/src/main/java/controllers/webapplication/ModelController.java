package controllers.webapplication;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import repositories.GalleryRepository;
import repositories.ModelRepository;


@Controller
@RequestMapping("/models")
public class ModelController {

	@Autowired
	private ModelRepository modelRepository;

	@Autowired
	private GalleryRepository galleryRepository;

	@GetMapping
	public String userGet(Model model) throws Exception {
		
		// galleries displayed in navbar dropdown-menu
		model.addAttribute("galleries", galleryRepository.findAll());
		
		boolean searchEnable = true;
		model.addAttribute("searchEnable", searchEnable);
		
		model.addAttribute("models", modelRepository.findAll());
		return "models";
	}
	

////	@GetMapping("/checklogin")
////	@ResponseBody
////	public ResponseEntity<String> checkLoginGet(@RequestParam("login") String login, Model model, HttpServletRequest request, HttpServletResponse response,
////			Locale locale) throws Exception {
////		if (userRepository.findByLogin(login).isPresent())
////			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
////		return new ResponseEntity<String>(HttpStatus.OK);
////	}
//	
//	
//
////	@PostMapping("/books/add")
////	public String addLivrePost(@Valid @ModelAttribute("livre") Livre livre, BindingResult br, Model model) {
////		new LivreValidator().validate(livre, br);
////		if (br.hasErrors())
////			return "addBook";
////		livreRepository.save(livre);
////		return "redirect:/books";
////	}
//	
////	@GetMapping("/add")
////	@ResponseBody
////	public ResponseEntity<String> addUserGet(Model model, HttpServletRequest request, HttpServletResponse response,
////			Locale locale) throws Exception {
////		model.addAttribute("newUser", new User());
////		return new ResponseEntity<String>(HttpStatus.OK);
////	}
//
////	@PostMapping("/add")
////	@ResponseBody
////	public ResponseEntity<String> addUserPost(@Valid @ModelAttribute("newUser") User user, BindingResult br,
////			Model model, HttpServletRequest request, HttpServletResponse response, Locale locale) throws Exception {
////		System.out.println("je passe par la validation du form utilisateur");
////
////		if (br.hasErrors()) {
////			System.out.println("erreur form add user");
////			String message = "Invalid add user form";
////			for (ObjectError e : br.getAllErrors())
////				message += e.toString();
////			return new ResponseEntity<String>(message, HttpStatus.BAD_REQUEST);
////		}
////
////		System.out.println("add user form ok");
////		User sessionUser = (User) request.getSession(false).getAttribute("user");
////		if (sessionUser != null) {
////			if (sessionUser.getRole() == RoleUtilisateur.Admin) {
////				user.setValidated(true);
////			}
////		} else {
////			user.setValidated(false);
////		}
////		utilisateurRepository.save(user);
////		model.addAttribute("users", Arrays.asList(user));
////		return new ResponseEntity<String>(HttpStatus.OK);
////	}
//
//	@GetMapping("edit")
//	@ResponseBody
//	public ResponseEntity<String> editUserGet(@RequestParam("id") int id, Model model, HttpServletRequest request, HttpServletResponse response, Locale locale) throws Exception {
//		Optional<User> u = userRepository.findById(id);
//		System.out.println(id);
//		if (!u.isPresent())
//			return new ResponseEntity<String>("No user found with id "+id, HttpStatus.BAD_REQUEST);
//		model.addAttribute("newUser", u.get());
//		return new ResponseEntity<String>(HttpStatus.OK);
//	}
//
//
//	@GetMapping("/rm")
//	@ResponseBody
//	public ResponseEntity<String> rmUser(@RequestParam("id") int id) {
//		userRepository.deleteById(id);
//		return new ResponseEntity<String>(HttpStatus.OK);
//	}
//
////	@GetMapping("/validate")
////	@ResponseBody
////	public ResponseEntity<String> validateUser(@RequestParam("id") int id, Model model, Locale locale,HttpServletRequest request) throws Exception{
////		if(!utilisateurRepository.findById(id).isPresent()) {
////			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
////		}
////		Utilisateur validatedUser = utilisateurRepository.findById(id).get();
////		
////		validatedUser.setValidated(true);
////
////		utilisateurRepository.save(validatedUser);
////		model.addAttribute("users", Arrays.asList(validatedUser));
////		
//////		List<Utilisateur> listUsers = (List<Utilisateur>) request.getAttribute("users");
//////		
//////		int count = 0;
//////		
//////		for(Utilisateur u : listUsers) {
//////			if(u.getId()==id) {
//////				listUsers.get(count).setValidated(true);
//////				break;
//////			}
//////			count++;
//////		}
////		return new ResponseEntity<String>(
////				viewRenderer.render("users", locale, model, request), HttpStatus.OK);
////			
////	}
	
}
