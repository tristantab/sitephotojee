package controllers.webapplication;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import utils.ViewRenderer;
import models.Commentary;
import models.User;
import repositories.CommentaryRepository;
import repositories.GalleryRepository;
import repositories.UserRepository;

@Controller
@RequestMapping("/commentaries")
public class CommentaryController {

	@Autowired
	private CommentaryRepository commentaryRepository;
	
//	@Autowired
//	private UserRepository userRepository;
	

	@Autowired
	private ViewRenderer viewRenderer;

	@Autowired
	private GalleryRepository galleryRepository;

	@GetMapping
	public String commentaryGet(Model model) throws Exception {
		
		// galleries displayed in navbar dropdown-menu
		model.addAttribute("galleries", galleryRepository.findAll());
		
		boolean searchEnable = true;
		model.addAttribute("searchEnable", searchEnable);
				
		model.addAttribute("commentaries", commentaryRepository.findAll());
		return "commentaries";
	}
		
	@GetMapping("/add")
	@ResponseBody
	public ResponseEntity<String> addUserGet(Model model, HttpServletRequest request, HttpServletResponse response,
			Locale locale) throws Exception {
		model.addAttribute("newCommentary", new Commentary());
		return new ResponseEntity<String>(viewRenderer.render("addCommentary", locale, model, request), HttpStatus.OK);
	}

	@PostMapping("/add")
	@ResponseBody
	public ResponseEntity<String> addUserPost(@Valid @ModelAttribute("newCommentary") Commentary commentary, BindingResult br,
			Model model, HttpServletRequest request, HttpServletResponse response, Locale locale) throws Exception {
		if (br.hasErrors()) {
			String message = "Invalid add commentary form";
			for (ObjectError e : br.getAllErrors())
				message += e.toString();
			return new ResponseEntity<String>(message, HttpStatus.BAD_REQUEST);
		}
		commentaryRepository.save(commentary);
			return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	@GetMapping("/edit")
	@ResponseBody
	public ResponseEntity<String> editUserGet(@RequestParam("id") int id, Model model, HttpServletRequest request, HttpServletResponse response, Locale locale) throws Exception {
		Optional<Commentary> c = commentaryRepository.findById(id);
		if (!c.isPresent())
			return new ResponseEntity<String>("No user found with id "+id, HttpStatus.BAD_REQUEST);
		model.addAttribute("newCommentary", c.get());
		return new ResponseEntity<String>(viewRenderer.render("addCommentary", locale, model, request), HttpStatus.OK);
	}

	@GetMapping("/rm")
	@ResponseBody
	public ResponseEntity<String> rmCommentary(@RequestParam("id") int id) {
		commentaryRepository.deleteById(id);
		return new ResponseEntity<String>(HttpStatus.OK);
	}


	
}
