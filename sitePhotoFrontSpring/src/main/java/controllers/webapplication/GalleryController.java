package controllers.webapplication;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import models.Gallery;
import models.Work;
import repositories.GalleryRepository;
import repositories.ImageRepository;

@Controller
@RequestMapping("/gallery")
public class GalleryController {

	@Autowired
	private ImageRepository imageRepository;
	
	@Autowired
	private GalleryRepository galleryRepository;
	

	@GetMapping 
	public String ImagesGetAll(@RequestParam("id") int id, Model model) {
		// galleries displayed in navbar dropdown-menu
		model.addAttribute("galleries", galleryRepository.findAll());
		
		boolean searchEnable = true;
		model.addAttribute("searchEnable", searchEnable);
		
		Optional<Gallery> opt = galleryRepository.findById(id);
		if (!opt.isPresent())
			return "no way Jose";
//			return new ResponseEntity<String>("No book found with id "+id, HttpStatus.BAD_REQUEST);
		else {
			model.addAttribute("galleryChosen", opt.get());
// 			// too slow ?
//			model.addAttribute("images", imageRepository.findAll());
			Gallery galleryChosenCompute = opt.get();
			Set<Work> imagesFromGal = new HashSet<Work>();
			for (Work workFromGal : galleryChosenCompute.getSetWorkGal()) {
				imagesFromGal.add(workFromGal);
			}
			model.addAttribute("images", imagesFromGal);
		}
		return "gallery";
	}
	
//	@GetMapping("/books/add")
//	public String addLivreGet(Model model) {
//		model.addAttribute("Livre", new Livre());
//		return "addBook";
//	}
//
//	@PostMapping("/books/add")
//	public String addLivrePost(@Valid @ModelAttribute("livre") Livre livre, BindingResult br, Model model) {
//		new LivreValidator().validate(livre, br);
//		if (br.hasErrors())
//			return "addBook";
//		livreRepository.save(livre);
//		return "redirect:/books";
//	}
//
//	@GetMapping("/books/rm")
//	public String rmBooksGet(@RequestParam("id") int id) {
//		livreRepository.deleteById(id);
//		return "redirect:/books";
//	}
	
}
