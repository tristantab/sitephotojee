package controllers.webapplication;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import models.User;
import repositories.UserRepository;


@Controller
public class AuthentificationController {

	@Autowired
	UserRepository userRepository;
	
	
	@GetMapping("login")
	@ResponseBody
	public ResponseEntity<String> loginGet(@Valid @ModelAttribute("user") User user, BindingResult br, Model model, Locale locale, HttpServletRequest request) throws Exception {
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	@PostMapping("login")
	@ResponseBody
	public ResponseEntity<String> login(@Valid @ModelAttribute("user") User user, BindingResult br, Model model, HttpServletRequest request) {
		List<User> results = userRepository.findByLoginAndPassword(user.getLogin(), user.getPassword());
		if (results.size() != 1) {
			return new ResponseEntity<String>("Wrong username or password", HttpStatus.NOT_FOUND);
		} else {
			request.getSession().setAttribute("connectedUser", results.get(0));
			return new ResponseEntity<String>(HttpStatus.OK);
		}
	}
	
	@GetMapping("logout")
	public String logout(HttpServletRequest request) {
		request.getSession().invalidate();
		return "redirect:/";
	}
	
//	@GetMapping("signup")
//	@ResponseBody
//	public ResponseEntity<String> signupGet( Model model) throws Exception {
//		model.addAttribute("newUser", new User());
//		return new ResponseEntity<String>(HttpStatus.OK);
//	}
//
//
//	@PostMapping("signup")
//	@ResponseBody
//	public ResponseEntity<String> signup(@Valid @ModelAttribute("user") User user, BindingResult br, Model model,  HttpServletRequest request) {
//		if (br.hasErrors()) {
//			return new ResponseEntity<String>("Wrong username or password", HttpStatus.NOT_FOUND);
//		} else {
//			System.out.println("user role = " + user.getUserRole() );
//			userRepository.save(user);
//			return new ResponseEntity<String>(HttpStatus.OK);
//		}
//	}
	
	
}
