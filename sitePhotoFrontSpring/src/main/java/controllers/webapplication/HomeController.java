package controllers.webapplication;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import models.Gallery;
import models.Image;
import models.Work;
import repositories.GalleryRepository;
//import validators.GalleryValidator;
import repositories.ImageRepository;

@Controller
public class HomeController {

	@Autowired
	private GalleryRepository galleryRepository;
	
	
	@GetMapping("/home") 
	public String GalleriesGetAll(Model model) {
		model.addAttribute("galleries", galleryRepository.findAll());
		
		boolean searchEnable = false;
		model.addAttribute("searchEnable", searchEnable);
		
		return "home";
	}
//	
//	// phase2 // used by admin only
//	@GetMapping("/home/add")
//	public String addLivreGet(Model model) {
//		model.addAttribute("Livre", new Gallery());
//		return "addGallery";
//	}

//	@PostMapping("/books/add")
//	public String addLivrePost(@Valid @ModelAttribute("livre") Livre livre, BindingResult br, Model model) {
//		new LivreValidator().validate(livre, br);
//		if (br.hasErrors())
//			return "addBook";
//		livreRepository.save(livre);
//		return "redirect:/books";
//	}

//	// phase2 // used by admin only
//	@GetMapping("/books/rm")
//	public String rmBooksGet(@RequestParam("id") int id) {
//		galleryRepository.deleteById(id);
//		return "redirect:/home";
//	}
	
}
