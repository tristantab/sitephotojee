package utils;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;

@Component
public class ViewRenderer {
	
	@Autowired
	@Qualifier("rendererViewResolver")
	private ViewResolver viewResolver;
	
	public String render(String viewName, Locale locale, Model model, HttpServletRequest request) throws Exception {
		View resolvedView = viewResolver.resolveViewName(viewName, locale);
		MockHttpServletResponse mockResp = new MockHttpServletResponse();
		resolvedView.render(model.asMap(), request, mockResp);
		return mockResp.getContentAsString();
	}

}
